export default {
    operators: [
        {
            name: 'sum',
            label: 'Сложить',
            isActive: true,
            validate: ['areNumbers'],
            do(a, b) {
                return Number(a) + Number(b)
            }
        },
        {
            name: 'substract',
            label: 'Вычесть',
            isActive: false,
            validate: ['areNumbers'],
            do(a, b) {
                return a - b
            }
        },
        {
            name: 'multiply',
            label: 'Умножить',
            isActive: true,
            validate: ['areNumbers', 'zeroWarning'],
            do(a, b) {
                return a * b
            }
        },
        {
            name: 'divide',
            label: 'Разделить',
            validate: ['areNumbers', 'areDividable', 'zeroWarning'],
            isActive: true,
            do(a, b) {
                return a / b
            }
        }
    ],
    validators: {
        areNumbers: {
            returnResult: false,
            validate(a,b) {
                return !isNaN(Number(a)) && !isNaN(Number(b))
            }
        },
        areDividable: {
            returnResult: false,
            validate(a,b) {
                return b != 0
            }
        },
        zeroWarning: {
            returnResult: true,
            validate(a,b) {
                return a != 0 && b != 0
            }
        }
    },
    messages: {
        areNumbers: 'Все значения должны быть числовыми',
        areDividable: 'Некорректная операция: на 0 делить нельзя',
        zeroWarning: 'Эта операция с 0 всегда возвращает 0'
    }
}
